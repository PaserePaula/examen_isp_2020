public class Aplicatie2 {


    public static void main(String[] args) {
        OThread1 oThread1=new OThread1("OThread1");
        OThread2 oThread2=new OThread2("OThread2");

    }

}


class OThread1 implements Runnable
{
    private Thread thread;

    OThread1(String name)
    {

        this.thread=new Thread(this);
        this.thread.setName(name);
        this.thread.start();
    }




    public void run() {

        for(int i=0;i<13;i++)
        {
            try {
                System.out.println(thread.getName()+"- "+Integer.toString(i+1) );
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}

class OThread2 implements Runnable
{
    private Thread thread;

    OThread2(String name)
    {

        this.thread=new Thread(this);
        this.thread.setName(name);
        this.thread.start();
    }


    public void run() {

        for(int i=0;i<13;i++)
        {
            try {
                System.out.println(thread.getName()+"- "+Integer.toString(i+1) );
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}